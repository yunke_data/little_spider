#-*- coding:utf-8 -*-

from splinter import Browser
from scrapy import Selector
from scrapy.http import HtmlResponse
import urllib2
import json
import hashlib
import codecs
import queue

def get_response(b,url):
	b.visit(url)
	bdata=b.html.encode("utf-8")
	response=HtmlResponse(url=url, body=bdata)
	return response

def parse_next(response):
	sel=Selector(response)
	url_n=sel.xpath("//li[@class='next']/a/@href").extract()
	url_next= url_n[0] if url_n else None
	return url_next



def parse_item(response):
	sel=Selector(response)
	con_div=sel.xpath("//div[@id='content']/div")
	for cdiv in con_div[:-1]:
		#print cdiv
		comName=cdiv.xpath("div[@class='shop-meta']/h3/a/text()").extract()
		comName=comName[0] if comName else ""
		comAddr=cdiv.xpath("div[@class='shop-meta']/span[@class='shop-meta__address']/@title").extract()
		comAddr=comAddr[0] if comAddr else ""
		comMob=cdiv.xpath("div[@class='shop-meta']/span[@class='shop-meta__phone']/text()").extract()
		comMob=comMob[0] if comMob else ""
		print comMob
		com_dict={"comName":comName,"comAddr":comAddr,"comMob":comMob}
		file_out="E:\\meituan\\"+hashlib.md5(repr(com_dict)).hexdigest()
		json.dump(com_dict, codecs.open(file_out,'w','utf-8'))



def main():
	#words_list=["中国","美容"]
	words_list=["DIY手工坊","KTV","VR","办公","文化用品","保龄球馆","壁球馆","别墅","采摘","农家乐","茶馆","产后塑形","超市","便利店","度假村","度假酒店","服饰鞋包","高端酒店","高尔夫场","公寓式酒店","购物频道","轰趴馆","花店","滑雪","化妆品","化妆品","家居建材","健身中心","京味儿购物","经济型","精品酒店","酒吧","酒店频道","咖啡厅","客栈旅舍","快捷酒店","篮球场","丽人频道","溜冰场","马术场","美发","美甲美睫","美容","SPA","密室","民宿","农家院","攀岩馆","漂流","品牌折扣店","乒乓球馆","棋牌室","亲子购物","青年旅舍","情侣酒店","祛痘","三星级","舒适型","商务酒店","射箭馆","食品茶酒","瘦身纤体","书店","数码产品","水果生鲜","私人影院","四星级","高档型","台球馆","台球馆","特色集市","体育场馆","网吧网咖","网球场","文化艺术","纹身","纹绣","五星级","豪华型","武术场馆","舞蹈","舞蹈","洗浴","休闲娱乐频道","眼镜店","药店","游乐游艺","游泳馆","瑜伽","瑜伽","羽毛球馆","运动户外","运动健身频道","真人CS","整形","中医养生","珠宝饰品","主题酒店","桌面游戏","综合商场","足疗按摩","足球场"]
	#b=Browser("phantomjs")
	q=queue.Queue()
	for wd in words_list:
		q.put(wd)
	print q.qsize()

	while q.qsize()>0:
		wd=q.get()
		print q.qsize()
		try:
			b=Browser("phantomjs")
			word=urllib2.quote(wd.decode("gbk").encode("utf-8"))
			url="http://bj.meituan.com/shops/?w="+word+"&mtt=1.index\%2Ffloornew.0.0.izi1pwpc" 
			print url
			#url="http://bj.meituan.com/shops/?w=%E4%B8%BD%E4%BA%BA&mtt=1.index%2Ffloornew.0.0.izi1pwpc"
			response=get_response(b, url)
			parse_item(response)
			url_next=parse_next(response)
			while url_next:
				response=get_response(b, url_next)
				parse_item(response)
				url_next=parse_next(response)
			b.quit()
		except:
			b.quit()
			#b=Browser("phantomjs")
			q.put(wd)

	b.quit()



main()
