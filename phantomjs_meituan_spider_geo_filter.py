#-*- coding:gbk -*-

from splinter import Browser
from scrapy import Selector
from scrapy.http import HtmlResponse
import urllib2
import json
import hashlib
import codecs
import queue
import time

def get_response(b,url):
	b.visit(url)
	time.sleep(4)
	bdata=b.html.encode("utf-8")
	response=HtmlResponse(url=url, body=bdata,encoding="utf-8")
	return response

def parse_geo_filter(response):
	sel=Selector(response)
	geolist=sel.xpath("//div[@class='filter-label-list filter-section geo-filter-wrapper  log-mod-viewed']/ul/li/a/@href").extract()
	return geolist


def parse_next(response):
	sel=Selector(response)
	url_n=sel.xpath("//li[@class='next']/a/@href").extract()
	url_next= url_n[0] if url_n else None
	return url_next



def parse_item(response):
	sel=Selector(response)
	con_div=sel.xpath("//div[@id='content']/div")
	for cdiv in con_div[:-1]:
		#print cdiv
		comName=cdiv.xpath("div[@class='shop-meta']/h3/a/text()").extract()
		comName=comName[0] if comName else ""
		comAddr=cdiv.xpath("div[@class='shop-meta']/span[@class='shop-meta__address']/@title").extract()
		comAddr=comAddr[0] if comAddr else ""
		comMob=cdiv.xpath("div[@class='shop-meta']/span[@class='shop-meta__phone']/text()").extract()
		comMob=comMob[0] if comMob else ""
		print comMob
		com_dict={"comName":comName,"comAddr":comAddr,"comMob":comMob}
		file_out="E:\\meituan\\My\\20170424_广州\\"+hashlib.md5(repr(com_dict)).hexdigest()
		json.dump(com_dict, codecs.open(file_out,'w','utf-8'))


def main_parse(b,response):
	parse_item(response)
	url_next=parse_next(response)
	while url_next:
		response=get_response(b, url_next)
		parse_item(response)
		url_next=parse_next(response)

def main():
	#words_list=["中国","美容"]
	#words_list=["DIY手工坊","KTV","VR","办公","用品","文化","文化用品","保龄球馆","壁球馆","别墅","采摘","农家乐","茶馆","产后塑形","超市","便利店","度假","村","度假","酒店","服饰","鞋包","高端","高尔夫","公寓式酒店","购物","频道","轰趴","公寓","花店","滑雪","化妆品","化妆品","家居","建材","健身","健身中心","购物","经济","精品","精品酒店","酒吧","酒店频道","咖啡厅","客栈","旅舍","快捷","快捷酒店","篮球场","丽人","溜冰场","马术场","美发","美甲","美睫","美容","SPA","密室","民宿","农家院","攀岩","攀岩馆","漂流","品牌","折扣店","乒乓球","棋牌室","亲子购物","青年","旅舍","情侣酒店","情侣","祛痘","星级","舒适型","商务酒店","射箭馆","食品","茶","酒","烟","瘦身","纤体","书店","数码","水果","生鲜","私人","影院","高档型","台球馆","台球馆","特色","集市","体育","场馆","网吧","网咖","网球场","文化","艺术","纹身","纹绣","五星级","豪华型","武术","舞蹈","洗浴","休闲","娱乐","眼镜店","药店","游乐","游艺","游泳馆","瑜伽","羽毛球","运动","户外","运动健身频道","真人CS","整形","中医","养生","珠宝","饰品","主题酒店","桌面游戏","综合商场","足疗","按摩","足球场"]
	#b=Browser("phantomjs")
	#words_list=["美容","养生","中医养生","美发","美容","SPA","美甲","美睫","纹绣","瘦身","纤体","纹身","祛痘","整形","产后塑形"]
	file_word="E:\\meituan\\meituan_text.json"
	words_list=json.load(open(file_word))
	words_list=[x.encode("gbk") for x in words_list]
	q=queue.Queue()
	for wd in words_list:
		q.put(wd)
	print q.qsize()
	b=Browser("chrome",wait_time=10)

	while q.qsize()>0:
		wd=q.get()
		print q.qsize()
		try:
			#b=Browser("phantomjs")
			#b=Browser("chrome")
			word=urllib2.quote(wd.decode("gbk").encode("utf-8"))
			#url="http://bj.meituan.com/shops/?w=%E4%B8%BD%E4%BA%BA&mtt=1.index%2Ffloornew.0.0.izi1pwpc"
			#url="http://bj.meituan.com/shops/all/all/avgscore?w="+word+"&mtt=1.index\%2Ffloornew.0.0.izi1pwpc" # 北京
			#url="http://www.meituan.com/shops/all/all/avgscore?w="+word+"&mtt=1.shops%2Fdefault.ze.2.j1ixxllm"
			url="http://gz.meituan.com/shops/all/all/avgscore?w="+word+"&mtt=1.shops%2Fdefault.ze.2.j0km8oij" #广州
			#url="http://gz.meituan.com/shops/?w="+word+"&mtt=1.index%2Ffloornew.0.0.izxqgnql"  # 广州
			print url

			response=get_response(b, url)
			geolist=parse_geo_filter(response)
			if geolist:
				for geo in geolist:
					response=get_response(b, geo)
					urllist=parse_geo_filter(response)
					if urllist:
						for url in urllist:
							response=get_response(b, url)
							main_parse(b,response)
					else:
						main_parse(b,response)
			else:
				main_parse(b,response)

			#parse_item(response)
			#url_next=parse_next(response)
			#while url_next:
			#	response=get_response(b, url_next)
			#	parse_item(response)
			#	url_next=parse_next(response)
			b.quit()
		#except Exception, e:
		except :
			#print e
			b.quit()
			#b=Browser("phantomjs")
			b=Browser("chrome",wait_time=10)
			q.put(wd)
			traceback.print_exc()

	b.quit()


import traceback
main()
