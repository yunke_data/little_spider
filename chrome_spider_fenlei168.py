#-*- coding:gbk -*-

import json
from splinter import Browser
from scrapy import Selector
from scrapy.http import HtmlResponse
from queue import Queue
import hashlib
import codecs, time
import os

def get_response(b,url):
	b.visit(url)
	time.sleep(2)
	#while not b.url.startswith(domain):
	while False:
		os.system("pause")
		b.visit(url)
		time.sleep(3)
	response=HtmlResponse(url=url, body=b.html.encode("utf-8"),encoding="utf-8")
	return response

def parse_list(response):
	sel=Selector(response)
	#urllist=sel.xpath('//body/div[@class="body1000"]/div[5]/div[@class="body1000"]/div[@class="body1000"]/div[@class="infolists"]/div[@class="list_simple"]/ul/div/span/a/@href').extract()
	urllist=sel.xpath('/html/body/div[@class="body1000"]/div[5]/div[@class="body1000"]/div[@class="body1000"]/div[@class="infolists"]/div[@class="section"]/ul/div/div/div[@class="media-body-title"]/a/@href').extract()
	return urllist 

def parse_item(response):
	ssel=Selector(response)
	title=ssel.xpath('//div[@class="information_title"]/text()').extract()
	print title
	title=title[0] if title else ""

	address=ssel.xpath('//div[@class="contact"]/ul/li[2]/text()').extract()
	address=";".join(address) if address else ""

	#qq=ssel.xpath('//div[@class="contact"]/ul/ul/li[1]/font[@class="tel"]/text()').extract()
	#qq=qq[0] if qq else ""

	klist=ssel.xpath('//div[@class="contact"]/ul/ul/li/span/text()').extract()
	vlist=ssel.xpath('//div[@class="contact"]/ul/ul/li/font[@class="tel"]/text()').extract()

	indict=dict(zip(klist[:-1], vlist))
	#	mob_response=get_response(b, )


	com_dict={
			"address":address,\
			"title":title
			}

	com_dict.update(indict)
	mx=ssel.xpath('//div[@class="contact"]/ul/ul/li[@class="qqbm"]/a/@onclick').extract()
	mob_url=mx[0].split("'")[-2] if mx else ""
	return com_dict, mob_url

def parse_mobile(response):
	msel=Selector(response)
	mobile=msel.xpath('//span[@class="num"]/text()').extract()
	mobile=mobile[0] if mobile else ""
	return mobile




def main():
	global domain
	domain="http://gz.fenlei168.com"
	#domain="http://bj.fenlei168.com"
	#path="/jiaoyu/"
	#path="/zhaoshangjiameng/"
	path="/shangwufuwu/"
	a_url=domain+path
	page_q=Queue()
	for i in range(1,800):
		page_q.put(a_url+"page-%d/" % i)

	print page_q.qsize()
	b=Browser("chrome",wait_time=6)
	while page_q.qsize()>0:
	        b_url=page_q.get()
		try:
			print b_url
	                response=get_response(b, b_url)
	                urllist=parse_list(response)
	                url_q=Queue()
	                for u in urllist:
	                	url_q.put(u)
	                while url_q.qsize()>0:
	                	c_url=url_q.get()
	                	try:
	                		mresponse=get_response(b, c_url)
	                	        com_dict, mob_url=parse_item(mresponse)
	                                mobile=""
	                                if mob_url:
	                                	mob_response=get_response(b, mob_url)
	                                	mobile=parse_mobile(mob_response)
	                                
	                                com_dict["mobile"]=mobile
	                                file_out="E:\\fenlei168\\20170414_bj\\"+hashlib.md5(repr(com_dict)).hexdigest()
	                                json.dump(com_dict, codecs.open(file_out,'w','utf-8'))
	                	except:
	                		b.quit()
	                		b=Browser("chrome",wait_time=6)
	                		url_q.put(c_url)
		except:
			b.quit()
			b=Browser("chrome",wait_time=6)
			page_q.put(b_url)
	b.quit()        






main()
