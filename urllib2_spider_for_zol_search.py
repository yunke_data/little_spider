#-*- coding:gbk -*-

import urllib2
from scrapy import Selector
from scrapy.http import HtmlResponse
import json
import queue
import cookielib
import time
import hashlib
import codecs
import re
import traceback

def create_opener():
	#time.sleep(3)
	enable_proxy=False
	if enable_proxy:
		proxy_handler=urllib2.ProxyHandler({"http":"http://damayi.com:8080"})
	else:
		proxy_handler=urllib2.ProxyHandler({})
	

	ckj=cookielib.CookieJar()
	cookie_handler=urllib2.HTTPCookieProcessor(ckj)
	#headers={
	#		"Accept":"text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8",\
	#		#"Accept-Encoding":"gzip, deflate, sdch",\
	#		"Accept-Language":"zh-CN,zh;q=0.8",\
	#		"Cache-Control":"max-age=0",\
	#		"Connection":"keep-alive",\
	#		"Host":"b.zol.com.cn",\
	#		#"If-Modified-Since":"Wed, 05 Apr 2017 10:22:14 GMT",\
	#		#"Referer":"http://b.zol.com.cn/qy/all",\
	#		"Referer":refer,\
	#		"Upgrade-Insecure-Requests":"1",\
	#		"User-Agent":"Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/53.0.2783.4 Safari/537.36"
	#		}
	opener=urllib2.build_opener(proxy_handler, cookie_handler)
	return opener

def get_response(opener,url, refer):
	print url
	headers={
			"Accept":"text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8",\
			#"Accept-Encoding":"gzip, deflate, sdch",\
			"Accept-Language":"zh-CN,zh;q=0.8",\
			"Cache-Control":"max-age=0",\
			"Connection":"keep-alive",\
			"Host":"b.zol.com.cn",\
			#"If-Modified-Since":"Wed, 05 Apr 2017 10:22:14 GMT",\
			#"Referer":"http://b.zol.com.cn/qy/all",\
			"Referer":refer,\
			"Upgrade-Insecure-Requests":"1",\
			"User-Agent":"Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/53.0.2783.4 Safari/537.36"
			}
	request=urllib2.Request(url=url, headers=headers)
	#data=opener.open(request, timeout=10).read().decode("gbk").encode("utf-8")
	data=opener.open(request, timeout=10).read().decode("gbk").encode("utf-8")
	#data=data.decode("utf8")
	response=HtmlResponse(url=url, body=data)
	return response

def parse_mulu_response(response):
	sel=Selector(response)
	info_sel_list=sel.xpath('//ul[@class="pro-list"]/li/div/div[@class="list-info"]')
	if info_sel_list:
		for info_sel in info_sel_list:
			title=info_sel.xpath('div[1]/h3/a/@title').extract()
			title=title[0] if title else ""
			main_produce=info_sel.xpath('div[@class="list-info-business"][1]/span[@class="business"]/text()').extract()
			main_produce=main_produce[0] if main_produce else ""
			contact=info_sel.xpath('div[@class="list-info-business clearfix"]/span[@class="business"]/span[1]/text()').extract()
			contact=contact[0] if contact else ""
			mobile=info_sel.xpath('div[@class="list-info-business clearfix"]/span[@class="business"]/span[2]/text()').extract()
			mobile=mobile[0] if mobile else ""

			address=info_sel.xpath('div[@class="list-info-business"][3]/span[@class="business"]/text()').extract()
			address=address[0] if address else ""
			com_dict={
					"title":title ,\
					"main_produce":main_produce,\
					"contact":contact,\
					"mobile":mobile,\
					"address":address,\
					"url":response.url
					}

			file_out="E:\\zol\\20170417_jh\\"+hashlib.md5(repr(com_dict)).hexdigest()
			json.dump(com_dict, codecs.open(file_out,'w','utf-8'))
		#next_page=sel.xpath('//div[@class="page"]/a/@href').extract()
		next_page=sel.xpath('//div[@class="page"]/a/text()').extract()
		#next_page=next_page[-1] if next_page else ""
		max_num=next_page[-2] if len(next_page)>=2 and next_page[-2]!="��һҳ".decode("gbk") else 1
		#return next_page
		print next_page
		return int(max_num)



def main():
	#url="http://b.zol.com.cn/qy/all"
	domain="http://b.zol.com.cn"
	#path="/qy/all"
	path="/qy/guangzhou/"
	url=domain+path
	opener=create_opener()
	zol_response=get_response(opener,url, domain)
	zol_sel=Selector(zol_response)
	zol_list=zol_sel.xpath('//div[@id="categoryContent"]/ul/li/div[@class="submenu"]/a/@href').extract()
	zol_q=queue.Queue()
	for z_url in zol_list[3:]:
		for i in range(1,4):
			zol_q.put(z_url+"?registerType=%d" % i)
	
	while zol_q.qsize() > 0:
		print zol_q.qsize()
		z_path=zol_q.get()
		try:
			z_url=domain+z_path
		        mulu_response=get_response(opener,z_url, url)
		        max_num=parse_mulu_response(mulu_response)
		        if max_num >1:
		        	page_q=queue.Queue()
		        	for i in range(1,max_num+1):
		        		str_i="_"+str(i)+"."
		        		n_url=re.sub(r"_[0-9]+\.",str_i,z_url)
		        		page_q.put(n_url)
		        	#print next_page,1
		        	while page_q.qsize()>0:
		        		n_url=page_q.get()
		        	        try:
		        	        	mulu_response=get_response(opener,n_url, url)
		        	        	parse_mulu_response(mulu_response)
		        	        	#print next_page,2
		        	        except UnicodeDecodeError:
		        	        	pass
		        	        except:
		        			traceback.print_exc()
		        	        	page_q.put(n_url)
		except:
			zol_q.put(z_path)




main()
