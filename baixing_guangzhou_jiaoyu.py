#-*- coding:utf-8 -*-

from scrapy import Selector
from scrapy.http import HtmlResponse
import urllib2
import queue
import time
from splinter import Browser
import os
import traceback
import cv2
import numpy as np
import os
import hashlib
import json
import codecs

def hanming(a_str, b_str):
	distance=0
	if len(a_str)!=len(b_str):
		raise ValueError, "the len of a_str/b_str is not equal"
	else:
		for i in range(len(a_str)):
			d=0 if a_str[i]==b_str[i] else 1
			distance=distance+d
	return distance

def get_img_print(data):
	mean=data.mean()
	dlist=[]
	for i in range(len(data)):
		for j in range(len(data[i])):
			code=0 if data[i][j]<mean else 1
			dlist.append(str(code))
	img_print="".join(dlist)
	return img_print

def get_image_print(n):
        
        def show(data):
        	cv2.imshow("data", data)
        	cv2.waitKey(0)
        
        def get_imglist(data):
        	imglist=[]
        	xStart=0
        	for _ in range(3):
        		xEnd=xStart+49
        		yStart=0
        		for _ in range(3):
        			yEnd=yStart+49
        			d=data[xStart:xEnd,yStart:yEnd]
        			imglist.append(d)
        			#show(d)
        			yStart=yEnd+2
        		xStart=xEnd+2
        	return imglist
        
        data=cv2.imread(img_path, 0)
        data=cv2.GaussianBlur(data,(3,3),0)
        
        imglist=get_imglist(data)
        img_match_dict={}
        numlist=[7,2,1,9,3,4,5,6,8]
        
        for i in range(len(imglist)):
        	img=imglist[i]
        	num=numlist[i]
        	img_8=cv2.resize(img,(n*8,n*8))
        	#img_8=cv2.resize(img,(64,64))
        	#show(img_8)
        	img_p=get_img_print(img_8)
        	img_match_dict[img_p]=num
        	#print img_p
        	#cv2.imwrite("E:\\yunke_fw\\"+"test8_"+str(i)+".png",img_8)
        	#cv2.imwrite("E:\\yunke_fw\\"+img_p+".png",img)
	return img_match_dict


def check_num(han_str, match_dict):
	d_min=9999
	v_num=9999
	for han, num in match_dict.iteritems():
		d=hanming(han_str, han)
		print "   ",d, num
		if d<d_min:
			d_min=d
			v_num=num
	return v_num, d
##################################################################
#def get_response(url):
#	time.sleep(3)
#	data=urllib2.urlopen(url, timeout=10).read()
#	#print data.decode("utf-8").encode("gbk")
#	#body=data.decode("").encode("utf-8")
#	response=HtmlResponse(url=url, body=data)
#	return response
def get_response(b,url):
	b.visit(url)
	time.sleep(3)
	response=HtmlResponse(url=url, body=b.html.encode("utf-8"))
	#if len(b.url) > 100:
	print url
	while b.url.endswith("spider_1"):
		print b.url, url
		os.system("pause")
		b.visit(url)
		time.sleep(4)
	response=HtmlResponse(url=url, body=b.html.encode("utf-8"))
	return response

def parse_mulu(response):
	q=queue.Queue()
	sel=Selector(response)
	urllist=sel.xpath('//form[@id="listing-filters"]/fieldset[2]/div/a/@href').extract()
	for url in urllist:
		q.put(url)
	return q

def is_num(x):
    try:
        t=int(x)
        return True
    except:
        return False

def parse_max_pagenum(response):
	sel=Selector(response)
	numlist=sel.xpath('//section[@class="listing-pager-section"]/ul/li/a/text()').extract()
	numlist=[int(x) for x in numlist if is_num(x)]
	num=max(numlist) if numlist else 1
	return num

def parse_pagelist(response):
	sel=Selector(response)
	urllist=sel.xpath("//div[@class='main']/ul[2]/li/div/div[@class='media-body-title']/a[1]/@href").extract()
	return urllist

def parse_item(response):
	sel=Selector(response)
	#itemsel=sel.xpath('//div[@class="viewad-content "]/div[@class="viewad-header"]/div[@class="viewad-title "]/h1/text()').extract()
	itemsel=sel.xpath('//div[@class="viewad-content "]')
	title=itemsel.xpath('div[@class="viewad-header"]/div[@class="viewad-title "]/h1/text()').extract()
	digsel=itemsel.xpath('div[@class="viewad-digest"]/div[@class="viewad-topMeta"]/section[@class="viewad-meta"]')
	edu_type=digsel.xpath('ul/li[2]/span/a/text()').extract()
	edu_type=edu_type[0] if edu_type else ""
	school=digsel.xpath('ul/li[3]/span/text()').extract()
	school=school[0] if school else ""
	contact=digsel.xpath('ul/li[4]/span/text()').extract()
	contact=contact[0] if contact else ""
	address=digsel.xpath('ul/li[5]/span//text()').extract()
	address="".join(address).replace(u'\xa0',"")
	mobile=sel.xpath('//p[@id="mobileNumber"]/strong/text()').extract()
	mobile=mobile[0] if mobile else ""
	
	com_dict={
			"title":title, \
			"edu_type":edu_type, \
			"school":school, \
			"contact":contact, \
			"address":address, \
			"mobile":mobile, \
			"url":response.url \
		}
	file_out="E:\\BaiXing\\beijing_20170427\\"+hashlib.md5(repr(com_dict)).hexdigest()
	json.dump(com_dict, codecs.open(file_out,'w','utf-8'))



def main():
	############################################
	n=1
	base_img="E:\\yunke_fw\\base\\baixing_tmp.png"
	#img_match_dict=get_image_print(n)
	############################################
	b=Browser("chrome")
	try:
		gz_url="http://beijing.baixing.com"
		#gz_url="http://guangzhou.baixing.com"
		#wd="/xuelipeixun/"
		#wd="/waiyupeixun/"
		#wd="/jinengpeixun/"
		#wd="/tiyupeixun/"
		#wd="/shejipeixun/"
		#wd="/wentipeixun/"
		#wd="/diannaopeixun/"
		#wd="/youerpeixun/"
		wd="/zxxjiaoyu/"
		#wd="/xuelipeixun/"
                jn_url=gz_url+wd
	        q=queue.Queue()

	        main_response=get_response(b,jn_url)
	        print jn_url 
	        print main_response
	        mulu_queue=parse_mulu(main_response)
	        print mulu_queue.qsize()
	        while mulu_queue.qsize() > 0:
			word=mulu_queue.get()
			try:
	        	        m_url=gz_url+word
	        	        son_response=get_response(b,m_url)
	        	        max_num=parse_max_pagenum(son_response)
				page_q=queue.Queue()
			        for i in range(1,max_num+1,1):
					page_q.put(i)
				while page_q.qsize()>0:
					m=page_q.get()
					try:
						xurl=m_url+"?page=%d" % m
						page_response=get_response(b,xurl)
						pagelist=parse_pagelist(page_response)
						url_q=queue.Queue()
						for url in pagelist:
							url_q.put(url)
						while url_q.qsize()>0:
							url=url_q.get()
							try:
								item_response=get_response(b,url)
								parse_item(item_response)
							except:
								b.quit()
								url_q.put(url)
								b=Browser("chrome")
								traceback.print_exc()
								#break
					except:
						b.quit()
						page_q.put(m)
						b=Browser("chrome")
						traceback.print_exc()
						#break
			except:
				b.quit()
				mulu_queue.put(word)
				b=Browser("chrome")
				traceback.print_exc()
				#break
				
			        	#print xurl
			        	#print m_url, max_num
			        #del son_response

	        #for i in range(1,101):
	        #	q.put(i)
	        #while q.qsize()>0:
	        #	n=q.get()
	        #	url=url+"?page=%d" % n
	        #	response=get_response(url)
	        #	#print response
	        #	sunlist=parse_pagelist(response)
	except:
		traceback.print_exc()
		b.quit()



main()



